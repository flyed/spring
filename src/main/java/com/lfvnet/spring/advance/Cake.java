package com.lfvnet.spring.advance;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
@Primary
public class Cake implements Dessert {
}
