package com.lfvnet.spring.advance;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
@ComponentScan
@PropertySource("classpath:/app.properties")
public class Config {
}
