package com.lfvnet.spring.advance;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
@Conditional(CookieCondition.class)
public class Cookies implements Dessert {
}
