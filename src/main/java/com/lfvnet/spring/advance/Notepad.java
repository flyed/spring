package com.lfvnet.spring.advance;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class Notepad {
}
