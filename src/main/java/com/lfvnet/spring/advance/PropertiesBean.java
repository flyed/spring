package com.lfvnet.spring.advance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
public class PropertiesBean {

    @Value("${name}")
    private String name;
    @Value("${age}")
    private Integer age;

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

}
