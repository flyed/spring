package com.lfvnet.spring.advance;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by zy on 17-7-12.
 */
@Configuration
@ComponentScan
public class ScopeConfig {
}
