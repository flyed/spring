package com.lfvnet.spring.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by zy on 17-7-12.
 */
@ContextConfiguration
@ComponentScan
@EnableAspectJAutoProxy
public class AopConfig {
}
