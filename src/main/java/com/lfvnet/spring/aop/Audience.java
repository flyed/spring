package com.lfvnet.spring.aop;

/**
 * Created by zy on 17-7-12.
 */

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Audience {

    @Around("perform()")
    public void watchPerformance(ProceedingJoinPoint jp){
        try{
            System.out.println("Silencing cell phones\n");
            System.out.println("Taking seats");
            jp.proceed();
            System.out.println("CLAP CLAP CLAP");
        }catch(Throwable e){
            System.out.println("Demanding a refund");
        }

    }

    /**
     * 定义一个切点
     */
    @Pointcut("execution(** com.lfvnet.spring.aop.Performance.perform(..))")
    public void perform(){}

    @Before("perform()")
    public void slienceCellphones(){
        System.out.println("Slience cell phones");
    }

    @Before("perform()")
    public void takeSeats(){
        System.out.println("Taking seats");
    }

    @AfterReturning("perform()")
    public void applause(){
        System.out.println("CLAP CLAP CLAP");
    }

    @AfterThrowing("perform()")
    public void demandRefund(){
        System.out.println("Demanding a refund");
    }
}
