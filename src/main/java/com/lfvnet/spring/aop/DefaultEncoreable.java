package com.lfvnet.spring.aop;

import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
public class DefaultEncoreable implements Encoreable {
    @Override
    public void performEncore() {

    }
}
