package com.lfvnet.spring.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;

/**
 * Created by zy on 17-7-12.
 */
@Aspect
public class EncoreableIntroducer {
    @DeclareParents(value="com.lfvnet.spring.aop.Performance+", defaultImpl = DefaultEncoreable.class)
    public static Encoreable encoreable;
}
