package com.lfvnet.spring.aop;

import org.springframework.stereotype.Component;

/**
 * Created by zy on 17-7-12.
 */
@Component
public class PerformA implements  Performance {
    @Override
    public void perform() {
        System.out.println("没有参数的perform");
    }

    @Override
    public void perform(String name) {
        System.out.println("有参数的perform");
    }
}
