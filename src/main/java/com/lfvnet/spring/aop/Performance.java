package com.lfvnet.spring.aop;

/**
 * Created by zy on 17-7-12.
 */
public interface Performance {
    void perform();
    void perform(String name);
}
