package com.lfvnet.spring.config;

import com.lfvnet.spring.soundSystem.CompactDisc;
import com.lfvnet.spring.soundSystem.SgtPeppers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Created by zy on 17-7-9.
 */
@Configuration
//@ComponentScan("com.lfvnet.spring.soundSystem") //如果没有参数，则会扫描本包及其子包
@PropertySource("classpath:/app.properties")
public class CDPlayConfig {

    @Autowired
    Environment env;

    @Bean
    public CompactDisc sgtPeppers(){
        return new SgtPeppers(env.getProperty("db.title"), env.getProperty("db.artlist"));
    }

}
