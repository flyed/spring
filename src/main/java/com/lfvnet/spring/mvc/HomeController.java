package com.lfvnet.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zy on 17-7-12.
 */
@Controller
public class HomeController {
    @RequestMapping(name = "/", method = RequestMethod.GET)
    public String home(){
        return "home";
    }
}
