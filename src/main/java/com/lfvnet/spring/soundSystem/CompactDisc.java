package com.lfvnet.spring.soundSystem;

/**
 * Created by zy on 17-7-9.
 */
public interface CompactDisc {
    void play();
}
