package com.lfvnet.spring.soundSystem;

/**
 * Created by zy on 17-7-9.
 */
public class SgtPeppers implements CompactDisc{

    private String title;
    private String artits;

    public SgtPeppers(String title, String artits) {
        this.title = title;
        this.artits = artits;
    }

    @Override
    public void play() {
        System.out.printf("Playing " + title + "by " + artits);
    }
}
