package com.lfvnet.spring.advance;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zy on 17-7-12.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=Config.class)
public class PropertiesTest {

    @Autowired
    PropertiesBean bean;

    @Test
    public void test01(){
        Assert.assertEquals(bean.getAge(), new Integer(12));
    }
}
