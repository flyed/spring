package com.lfvnet.spring.advance;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zy on 17-7-12.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ScopeConfig.class)
public class ScopeTest {
    @Autowired
    Notepad notepad01;
    @Autowired
    Notepad notepad02;

    @Test
    public void test01(){
        //Assert.assertFalse(notepad01==notepad02);
    }
}
