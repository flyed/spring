package com.lfvnet.spring.advance;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zy on 17-7-12.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Config.class)
public class TestAdvance {

    @Autowired
    //@Qualifier("iceCream")
    Dessert dessert;

    @Test
    public void test01(){
        Assert.assertNotNull(dessert);
    }

    @Test
    public void test02(){
        Assert.assertNotEquals(dessert.getClass().getSimpleName(), "IceCream");
    }

    @Test
    public void test03(){
        Assert.assertEquals(dessert.getClass().getSimpleName(), "Cake");
    }
}
