package com.lfvnet.spring.mvc;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by zy on 17-7-12.
 */
public class HomeControllerTest {

    @Test
    public void test01(){
        HomeController homeController = new HomeController();
        Assert.assertEquals("home", homeController.home());
    }

    @Test
    public void test02() throws Exception {
        HomeController homeController = new HomeController();
        MockMvc mockMvc = standaloneSetup(homeController).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(view().name("home"));
    }
}
