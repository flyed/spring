package com.lfvnet.spring;

import com.lfvnet.spring.config.CDPlayConfig;
import com.lfvnet.spring.soundSystem.CDPlayer;
import com.lfvnet.spring.soundSystem.CompactDisc;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zy on 17-7-9.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CDPlayConfig.class})
public class test {

    @Autowired
    CompactDisc compactDisc;

    @Autowired
    CDPlayer cdPlayer;

    @Test
    public void testSayHello(){
        HelloBean helloBean = new HelloBean();
        helloBean.sayHello();
    }

    @Test
    public void cdShouldNotNull(){
        Assert.assertNotNull(compactDisc);
    }

    @Test
    public void testCDPlayer(){
        Assert.assertNotNull(cdPlayer);
    }

}
